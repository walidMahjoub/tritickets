<?php
/**
 * Created by PhpStorm.
 * User: Walid-MAHJOUB
 * Date: 20/02/2018
 * Time: 16:32
 */

namespace Models;

use Ressources\TicketRessource;

class Ticket
{

    /**
     * @param $from
     * @param $to
     * @return array
     * @throws \Exception
     */
    public function getOrderedLisOfTickets($from, $to)
    {
        $ticketRessources= new TicketRessource();
        $orderedTickets = $ticketRessources->getOrderedLisOfTickets($from,$to);

        return $this->getArrayOfTextsToPrint($orderedTickets);
    }

    public function getArrayOfTextsToPrint($orderedTickets)
    {
        $arrayOfTexts =array();
        foreach ($orderedTickets as $ticket) {
            $text = 'prendre le ' . $ticket['means'] . ' de ' . $ticket['from'] . ' pour ' . $ticket['to'] . '. ' ;
            if(isset($ticket['door'])){
                $text . $ticket['door'] .', ' ;
            }
            (isset($ticket['seat']))? $text .= $ticket['seat'] : $text .= ' Pas de siège réservé';

            if(isset($ticket['informations'])){
                $text.= ' ' . $ticket['informations'];
            }
            $arrayOfTexts[] =$text;
        }

        $arrayOfTexts[] = 'Vous êtes arrivé à destination.';
        return $arrayOfTexts;
    }

}
