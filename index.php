<form method="get">
    <input type="text" name="from" placeholder="départ">
    <input type="text" name="to" placeholder="arrivé">
    <input type="submit">
</form>

<?php

require_once 'autoload.php';
try {
    $from = '';
    $to = '';
    if (isset($_GET['from'])) {
        $from = $_GET['from'];
    }
    if (isset($_GET['to'])) {
        $to = $_GET['to'];
    }

    if ($to != '' && $from != '') {
        $tick = new \Models\Ticket();
        $orderedList = $tick->getOrderedLisOfTickets($from, $to);
        $rang = 1;
        foreach ($orderedList as $ticket) {
            echo $rang . '- ' . $ticket . '<br/>';
            $rang++;
        }
    }

} catch (Exception $e) {
    echo $e->getMessage();
}









