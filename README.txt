Enonc� :
Vous avez dans votre poche les 4 titres de transport suivants :
- Ticket de bus Paris-Orly
- Billet d'avion Stockholm-New York (vol SK57, si�ge 8D. Embarquement porte 24. Les bagages seront automatiquement transf�r�s depuis votre derni�re �tape.)
- Billet d'avion Paris-Stockholm (vol SK455, si�ge 3A. Embarquement porte 45B. D�p�t de bagage au guichet 344)
- Billet de train Montpellier-Paris (train n� 7895, si�ge 95D)
Structurez ces donn�es sous la forme d'un tableau associatif multi-dimensionnel.
Ecrivez une API qui vous permettra, en fonction d'une provenance et d'une destination donn�es, de trier
une liste non ordonn�e de titres de transport et pr�sentera en retour l?organisation de votre voyage.
Exemple : Vous partez de Montpellier pour aller � New York. L?API doit produire cette liste en retour :
1. Prendre le train 7895 de Montpellier � Paris. S?assoir au si�ge 95D.
2. Prendre le bus de Paris � l?a�roport Paris-Orly. Pas de si�ge r�serv�.
3. De l?a�roport Paris-Orly, prendre le vol SK455 pour Stockholm. Porte 45B, si�ge 3A. D�p�t de
bagage au guichet 344.
4. De Stockholm, prendre le vol SK57 pour New York JFK. Porte 24, si�ge 8D. Les bagages seront
automatiquement transf�r�s depuis votre derni�re �tape.
5. Vous �tes arriv� � destination.
