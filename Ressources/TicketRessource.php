<?php
/**
 * Created by PhpStorm.
 * User: Walid-MAHJOUB
 * Date: 20/02/2018
 * Time: 15:11
 */

namespace Ressources;


class TicketRessource
{
    /**
     * @var array les tickets de transports
     */
    private static $tickets = array(
        array(
            'from' => 'Paris',
            'to' => 'l’aéroport Paris-Orly',
            'means' => 'bus',
        ),
        array(
            'from' => 'Stockholm',
            'to' => 'New York',
            'means' => 'vol SK57',
            'seat' => 'siège 8D',
            'door' => 'porte 24',
            'informations' => 'Les bagages seront automatiquement transférés depuis votre dernière étape'
        ),
        array(
            'from' => 'l’aéroport Paris-Orly',
            'to' => 'Stockholm',
            'means' => 'vol SK455',
            'seat' => 'siège 3A',
            'door' => 'porte 45B',
            'informations' => 'Dépôt de bagage au guichet 344'
        ),
        array(
            'from' => 'Montpellier',
            'to' => 'Paris',
            'means' => 'train 7895',
            'seat' => 'siège 95D',
        ),
    );


    /**
     * @param $from
     * @param $to
     * @return array
     * @throws \Exception
     */
    public function getOrderedLisOfTickets($from, $to)
    {
        $tickets = self::$tickets;

        $first = $this->getFirst($tickets, $from);
        $latest = $this->getLatest($tickets, $to);

        if (!$first) {
            throw new \Exception('Point de départ non trouvé');
        }

        if($first['to'] == $to){
            $orderedList[] = $first;
            return $orderedList;
        }

        if (!$latest) {
            throw new \Exception('Point d\'arrivé non trouvé');
        }

        if($first['to'] == $latest['from']){
            $orderedList = array($first,$latest);
            return $orderedList;
        }

        $orderedList[] = $first;

        $this->completeList($orderedList, $tickets, $latest);

        return $orderedList;

    }

    private function getFirst(&$tickets, $from)
    {
        $first = false;
        foreach ($tickets as $index => $ticket) {
            if ($ticket['from'] == $from) {
                $first = $ticket;
                unset($tickets[$index]);
                break;
            }
        }
        return $first;
    }

    private function getLatest(&$tickets, $to)
    {
        $latest = false;
        foreach ($tickets as $index => $ticket) {
            if ($ticket['to'] == $to) {
                $latest = $ticket;
                unset($tickets[$index]);
                break;
            }
        }
        return $latest;
    }

    private function completeList(&$orderedList, &$tickets, $latest)
    {
        $endOfOrdered = end($orderedList);
        foreach ($tickets as $index => $ticket) {
            if ($ticket['from'] == $endOfOrdered['to']) {
                $orderedList[] = $ticket;
                if ($ticket['to'] == $latest['from']) {
                    $orderedList[] = $latest;
                } else {
                    unset($tickets[$index]);
                    $this->completeList($orderedList, $tickets, $latest);
                }
            }

        }
    }


}